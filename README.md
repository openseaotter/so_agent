# 简介

OpenSeaOtter Agent用于订阅OpenSeaOtter Server的变更信息，从而实现镜像的持续部署。

![arch](./images/arch.png)

# 安装和使用

## 安装

1. 从[这里](https://gitcode.com/openseaotter/so_agent/releases)下载最新的版本
2. 在linux上以root给文件赋予执行权限

```bash
# chmod a+x ./so_agent
```
3. 在linux以root执行安装

```bash
# ./so_agent service install
```

## 启动

1. 在linux以root执行启动

```bash
# ./so_agent service start
```

2. 在linux以root用户检查是否启动成功

```bash
# ./so_agent service status
```
## 生成变更订阅配置

这步需要使用SeaOtter Server(需要 0.1.1以上版本)命令。

```bash
# /usr/sbin/seaotter watch create [group1/image1] ... [groupn/imagen]
```
group1/image1,groupn/imagen 需要替换成你需要的镜像。这些镜像需要在镜像仓库中存在。

在创建订阅配置后，需要查看是否已经启用订阅配置。

```bash
# /usr/sbin/seaotter watch list
```

如果配置未启用，可以执行如下命令

```bash
# /usr/sbin/seaotter watch enable [watch id]
```

## 生成执行脚本

SeaOtter Agent只用于接收变更通知和执行对应的脚本。你可以生成脚本模板，然后再进行修改。

```bash
# /usr/sbin/seaotter_agent script gen [desc file]
```
目标文件支持.py(python)和.js(nodejs)结尾。

## 测试执行脚本
你可以执行执行脚本，也可以使用script test子命令

```bash
# /usr/sbin/seaotter_agent script test --beforeChange [before Change] --afterChange [after change] --unChange [unchange] [script file]
```
| 参数           | 说明                                |
| :------------- | :---------------------------------- |
| --beforeChange | 表示变化前的镜像名                  |
| --afterChange  | 表示变化和的镜像名                  |
| --unChange     | 表示未变更的镜像名称 ，可以指定多次 |

## 订阅变更

以root用户执行下面命令

```bash
# /usr/sbin/seaotter_agent config addWatch --serverAddr [serverAddr] [watch id] [secret] [script file]
```
| 参数         | 说明                  |
| :----------- | :-------------------- |
| --serverAddr | SeaOtter Server的地址 |

# 使用图形界面

TODO
