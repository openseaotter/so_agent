// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package main

import (
	"fmt"
	"os"

	"gitcode.com/openseaotter/so_agent/cmds/config_cmd"
	"gitcode.com/openseaotter/so_agent/cmds/script_cmd"
	"gitcode.com/openseaotter/so_agent/cmds/service_cmd"
	"github.com/spf13/cobra"
)

const (
	VERSION = "0.1.1"
)

var versionCmd = &cobra.Command{
	Use: "version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("OpenSeaOtter Agent version", VERSION)
	},
}

func main() {
	var rootCmd = &cobra.Command{}
	rootCmd.AddCommand(config_cmd.ConfigCmd, service_cmd.ServiceCmd, script_cmd.ScriptCmd, versionCmd)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
