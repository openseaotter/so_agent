module gitcode.com/openseaotter/so_agent

go 1.22.5

require (
	gitcode.com/openseaotter/so_proto_gen_go.git v0.0.0-20250121024157-1b04ba07495b
	github.com/spf13/cobra v1.8.1
	google.golang.org/grpc v1.69.4
	gopkg.in/yaml.v3 v3.0.1
)

require go.uber.org/multierr v1.10.0 // indirect

require (
	github.com/dchest/uniuri v1.2.0
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	go.uber.org/zap v1.27.0
	golang.org/x/net v0.30.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/text v0.19.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20241015192408-796eee8c2d53 // indirect
	google.golang.org/protobuf v1.36.2 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)
