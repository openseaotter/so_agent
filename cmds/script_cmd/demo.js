#!/usr/bin/env node

const fs = require('fs');

function getChangeInfo() {
  //直接执行脚本调试，你可以修改下面测试数据
  if (process.argv.length == 2) {
    return {
      changeId: "test",
      watchId: "test",
      timeStamp: 1737353419452,
      changeFromUrl: "xx:1.0",
      changeToUrl: "xx:2.0",
      changeFromDigest:
        "sha256:62bb7ec2c8535b23c3d529d35236515f57c30cf0af2aebcf3157de55e2e07940",
      changeToDigest:
        "sha256:c0f766b29ee240acf5017ac91388a6aead749bd6605162cd92f58a9ab50f25e5",
      unChangeUrlList: ["zz:1.0"],
    };
  } else if (process.argv.length == 3) {
    const content = fs.readFileSync(process.argv[2], "utf8");
    return JSON.parse(content);
  }
}

//获取变更信息
const changeInfo = getChangeInfo();
console.log(changeInfo);

//后续操作
