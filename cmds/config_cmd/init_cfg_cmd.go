// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package config_cmd

import (
	"fmt"
	"os"
	"os/user"

	"gitcode.com/openseaotter/so_agent/config"
	"gitcode.com/openseaotter/so_agent/utils"
	"github.com/spf13/cobra"
)

var initCfgCmd = &cobra.Command{
	Use: "init",
	RunE: func(cmd *cobra.Command, args []string) error {
		//检查是否是root用户
		curUser, err := user.Current()
		if err != nil {
			return err
		}
		if curUser.Uid != "0" {
			return fmt.Errorf("need root to run this command")
		}

		_, err = os.Stat(utils.CONFIG_FILE_PATH)
		if err == nil {
			return fmt.Errorf("config already exist")
		}

		cfg := &config.AgentConfig{
			MaxExecSecond: 60,
			WatchCfgList:  []config.WatchConfig{},
		}
		err = config.WriteAgentConfig(cfg)
		if err != nil {
			return err
		}
		fmt.Println("init config success")
		return nil
	},
}
