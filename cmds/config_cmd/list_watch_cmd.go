// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package config_cmd

import (
	"fmt"

	"gitcode.com/openseaotter/so_agent/config"
	"github.com/spf13/cobra"
)

var listWatchCmd = &cobra.Command{
	Use: "listWatch",
	RunE: func(cmd *cobra.Command, args []string) error {
		cfg, err := config.ReadAgentConfig()
		if err != nil {
			return err
		}
		fmt.Println("watch config count", len(cfg.WatchCfgList))
		for _, watchCfg := range cfg.WatchCfgList {
			fmt.Printf("%s serverAddr:%s scriptFile:%s\n", watchCfg.WatchId, watchCfg.ServerAddr, watchCfg.ScriptFile)
		}
		return nil
	},
}
