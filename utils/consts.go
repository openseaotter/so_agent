// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package utils

const CONFIG_DIR_PATH = "/etc/seaotter"
const CONFIG_FILE_PATH = "/etc/seaotter/agent.yaml"

const SERVICE_FILE_PATH = "/usr/lib/systemd/system/seaotter_agent.service"

const DATA_DIR_PATH = "/var/lib/seaotter" //本地存储
const PID_FILE_PATH = "/var/lib/seaotter/seaotter_agent.pid"

const EXEC_FILE_PATH = "/usr/sbin/seaotter_agent"

const LOG_FILE_PATH = "/var/log/seaotter_agent.log"
