// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

package utils

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strings"
)

func GenSign(timeStamp int64, randomStr, secret string) string {
	toSignStr := fmt.Sprintf("%d%s%s", timeStamp, randomStr, secret)
	h := sha256.New()
	h.Write([]byte(toSignStr))
	signData := h.Sum(nil)
	return strings.ToLower(hex.EncodeToString(signData))
}
